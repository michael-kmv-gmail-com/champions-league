import { createApp } from "vue";

import Simulation from "./views/Simulation.vue";

createApp(Simulation).mount("#app");

require("./bootstrap");
