# Insider (Formerly SOCIAPlus) Champions League #

In this project, we expect you to complete a simulation. In this simulation, there will be a group of
football teams and the simulation will show match results and the league table. Your task is to estimate
the final league table.

## Setup ##

### Apache ###
- Require mod_rewrite module

### PHP ###
- 7.3.20 or later

### npm ###
- Run `sudo apt-get install npm`

### Project ###
- Clone the GitHub project
- Copy the `.env.example` and rename it to `.env`
- Create application database ("champions_league_simulation", for example)
- Config your local .env file. You should set up a database and make it connect to the DB mentioned in `.env`.
- Run `composer install`
- Run `php artisan migrate`
- Run `php artisan db:seed`
- Run `php artisan serve` for development
- Run `npm install`
- Run `npm run watch` for development or `npm run prod` for production
- Open http://127.0.0.1:8000/ in your browser

### Cache ###
- Clear cache command: `php artisan optimize:clear`

### Tests ###
- Create test database
- Config your local .env.testing file. You should set up a database and make it connect to the DB mentioned in `.env.testing`.
- Run `php artisan test` OR `vendor/bin/phpunit`

### Swagger Open API documentation ###
- Location: `http://your_app_domain_and_port/api/documentation` (http://127.0.0.1:8000/api/documentation)
- L5_SWAGGER_GENERATE_ALWAYS=true in .env for auto generation after documentation request

### API ###
Headers
- Accept application/json - for all requests

