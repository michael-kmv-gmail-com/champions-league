<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Containers\Simulation\Tasks\DrawForTeams;

class DrawForTeamsTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testDrawForTeams()
    {
        $commandsIds = [1, 2];
        $drawForTeams = resolve(DrawForTeams::class);
        $draw = $drawForTeams->draw($commandsIds);

        $expected = [[1,2], [2,1]];
        $this->assertEqualsCanonicalizing($draw, $expected);
    }
}
