<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SimulationTest extends TestCase
{
    public function testRunSimulationCorrectly()
    {
        $response = $this->json('POST', '/api/simulation-run')
            ->assertStatus(200)
            ->assertJsonStructure([
                    'id', 'name', 'weekly_game_plan',
            ]);
    }
}
