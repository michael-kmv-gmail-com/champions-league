<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Command;

class CommandsTest extends TestCase
{
    public function testGetCommandsListCorrectly()
    {
        $response = $this->json('GET', '/api/commands')
            ->assertStatus(200)
            ->assertJsonStructure([
                    '*' => ['id', 'name', 'strength'],
            ]);
    }

    public function testAddCommandSuccessfully()
    {
        $payload = ['name' => 'new command 1', 'strength' => '5'];

        $this->json('POST', 'api/command', $payload)
            ->assertStatus(200)
            ->assertJsonStructure([
                'id',
                'name',
                'strength'
            ]);
    }

}
