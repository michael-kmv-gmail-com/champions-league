<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Game;

class GamesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Game::truncate();
        Game::unguard();

        Game::create([
            'id' => 1,
            'season_id'=> 1,
            'week_num' => 1,
            'command1' => 1,
            'command2' => 2,
            'command1_goals_scored' => 3,
            'command2_goals_scored' => 2,
        ]);
        Game::create([
            'id' => 2,
            'season_id'=> 1,
            'week_num' => 1,
            'command1' => 3,
            'command2' => 4,
            'command1_goals_scored' => 1,
            'command2_goals_scored' => 4,
        ]);
    }
}
