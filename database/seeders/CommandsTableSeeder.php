<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Command;

class CommandsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Command::truncate();
        Command::unguard();

        Command::create([
            'id' => 1,
            'name' => 'Chelsea',
            'strength' => 5,
        ]);
        Command::create([
            'id' => 2,
            'name' => 'Arsenal',
            'strength' => 5,
        ]);
        Command::create([
            'id' => 3,
            'name' => 'Manchester City',
            'strength' => 5,
        ]);
        Command::create([
            'id' => 4,
            'name' => 'Liverpool',
            'strength' => 5,
        ]);

    }
}
