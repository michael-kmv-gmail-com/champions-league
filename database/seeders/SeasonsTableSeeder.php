<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Season;

class SeasonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Season::truncate();
        Season::unguard();

        Season::create([
            'id' => 1,
            'name' => '2020',
            'weekly_game_plan' => '{"season_name":"2021-11-28 08:26:04","weekly_game_plan":[{"match":[[2,1],[4,3]]},{"match":[[3,2],[1,4]]},{"match":[[3,4],[1,2]]},{"match":[[2,4],[3,1]]},{"match":[[4,2],[1,3]]},{"match":[[2,3],[4,1]]}]}'
        ]);

    }
}
