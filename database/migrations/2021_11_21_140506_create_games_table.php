<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Season;
use App\Models\Command;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            //$table->integer('season_id')->index('season_id');
            $table->foreignId('season_id')->constrained('seasons');
            $table->integer('week_num')->index('week_num');
            $table->foreignId('command1')->constrained('commands');
            $table->foreignId('command2')->constrained('commands');
            //$table->integer('command1')->index('command1');
            //$table->integer('command2')->index('command2');
            $table->tinyInteger('command1_goals_scored');
            $table->tinyInteger('command2_goals_scored');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}
