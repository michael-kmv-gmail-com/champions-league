<?php

namespace App\Http\Helpers;

class ApiResponseDataHelper
{
    /**
     * Success response method
     *
     * @param $result
     * @param string $message
     * @param int $code
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendResponse($result, $message = '', $code = 200)
    {
        /*
        $response = [
            'success' => true,
            'data'    => $result,
            'message' => $message,
        ];
        */
        $response = $result;

        return response()->json($response, $code);
    }

    /**
     * Return error response
     *
     * @param $error
     * @param array $errorMessages
     * @param int $code
     * @return \Illuminate\Http\JsonResponse
     */
    public static function sendError($error, $errorMessages = [], $code = 404)
    {
        $response = [
            'message' => $error,
        ];

        if (!empty($errorMessages)) {
            $response['errors'] = $errorMessages;
        }

        $res = array_merge(['Content-Type => application/json'], \App\Http\Middleware\AccessControlAllowOrigin::ACCESS_CONTROL_HEADERS);
        return response($response, $code)->withHeaders($res);

        //return response()->json($response, $code);
    }
}
