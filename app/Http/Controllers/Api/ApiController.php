<?php

namespace App\Http\Controllers\Api;

use App\Http\Helpers\ApiResponseDataHelper;
use App\Http\Controllers\Controller;

/**
 * @OA\Info(
 *     version="1.0.0",
 *     title="Insider (Formerly SOCIAPlus) Champions League API Documentstion",
 *     description="Insider (Formerly SOCIAPlus) Champions League API Documentstion: Swagger OpenApi description",
 *     @OA\Contact(
 *         email="michael.kmv@gmail.com"
 *     ),
 *     @OA\License(
 *         name="Apache 2.0",
 *         url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *     )
 * )
 *
 * @OA\Tag(
 *     name="General",
 *     description="The general actions list",
 * )
 * @OA\Tag(
 *     name="Commands",
 *     description="The general team actions",
 * )
 * @OA\Tag(
 *     name="Simulation",
 *     description="The general actions with simulation",
 * )
 *
 * @OA\Server(
 *      url="http://127.0.0.1:8000/api",
 *      description="Champions League Swagger OpenApi server"
 * )
 */

class ApiController extends Controller
{
    /**
     * @var ApiResponseDataHelper
     */
    protected $responseDataHelper;

    public function __construct(ApiResponseDataHelper $responseDataHelper)
    {
        // Response helper
        $this->responseDataHelper = $responseDataHelper;
    }
}
