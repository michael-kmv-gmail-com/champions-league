<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\ApiController;
use Illuminate\Http\Request;
use App\Http\Requests\AddCommandRequest;
use App\Containers\Commands\Actions\CommandsList;
use App\Containers\Commands\Actions\CommandsStore;
use App\Containers\Commands\Dto\CreateCommandDtoFactory;
use App\Http\Resources\CommandsList as CommandsListResource;
use App\Http\Resources\Command as CommandResource;

class CommandsController extends ApiController
{
    /**
     * @OA\Get(
     *     path="/commands",
     *     operationId="CommandsController.index",
     *     tags={"Commands"},
     *     summary="Display a listing of commands",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Everything is fine",
     *         @OA\JsonContent(),
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Resource not found"
     *     )
     * )
     *
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $commandsList = app(CommandsList::class)->getAllCommandsList();
        return $this->responseDataHelper->sendResponse(CommandsListResource::collection($commandsList), 'Commands List retrieved successfully.');
    }

    /**
     * @OA\Post(
     *     path="/command",
     *     operationId="CommandsController.store",
     *     tags={"Commands"},
     *     summary="Add (save) new command to the DB",
     *     @OA\RequestBody(
     *         description="Command details",
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(ref="#/components/schemas/AddCommandRequest")
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Everything is fine",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="422",
     *         description="The given data was invalid"
     *     )
     * )
     *
     * Save command
     *
     * @param AddCommandRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(AddCommandRequest $request)
    {
        $dto = CreateCommandDtoFactory::fromRequest($request);
        $command = app(CommandsStore::class)->saveCommand($dto);
        return $this->responseDataHelper->sendResponse(new CommandResource($command), 'Command created successfully.');
    }

}
