<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Requests\PlaySimulationNextWeekRequest;
use App\Containers\Simulation\Actions\SimulationInit;
use App\Containers\Simulation\Actions\SimulationPlay;
use App\Containers\Simulation\Dto\CreatePlaySimulationNextWeekDtoFactory;

class SimulationController extends ApiController
{
    /**
     * @OA\Post(
     *     path="/simulation-run",
     *     operationId="SimulationController.run",
     *     tags={"Simulation"},
     *     summary="Run simulation",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Everything is fine",
     *         @OA\JsonContent(),
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Resource not found"
     *     )
     * )
     *
     * Run simulation to prepare data.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function run()
    {
        $simulation = app(SimulationInit::class)->initSumulation();
        return $this->responseDataHelper->sendResponse($simulation, 'Simulation run executed successfully.');
    }

    /**
     * @OA\Post(
     *     path="/simulation-play-next-week",
     *     operationId="SimulationController.playNextWeek",
     *     tags={"Simulation"},
     *     summary="Play simulation next week",
     *     @OA\RequestBody(
     *         description="Command details",
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(ref="#/components/schemas/PlaySimulationNextWeekRequest")
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Everything is fine",
     *         @OA\JsonContent(),
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Resource not found"
     *     )
     * )
     *
     * Play simulation next week
     *
     * @param PlaySimulationNextWeekRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function playNextWeek(PlaySimulationNextWeekRequest $request)
    {
        $dto = CreatePlaySimulationNextWeekDtoFactory::fromRequest($request);

        if (!app(SimulationPlay::class)->isValidSimulationPlayData($dto)) {
            return $this->responseDataHelper->sendError('Invalid simulation play data.', ['error'=>'Invalid simulation play data']);
        }

        $simulation = app(SimulationPlay::class)->playSumulationNextWeek($dto);
        return $this->responseDataHelper->sendResponse($simulation, 'Simulation run executed successfully.');
    }

}
