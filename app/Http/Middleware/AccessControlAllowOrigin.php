<?php

namespace App\Http\Middleware;

use Closure;

class AccessControlAllowOrigin
{
    const ACCESS_CONTROL_HEADERS = [
        "Access-Control-Allow-Origin" => '*',
        "Access-Control-Allow-Credentials" => true,
        "Access-Control-Allow-Methods" => 'GET, POST, OPTIONS',
        "Vary" => "Origin"
    ];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        if (!app()->environment('testing')) {
            foreach (self::ACCESS_CONTROL_HEADERS as $key => $val)
                $response->headers->set($key, $val);
        }

        return $response;
    }
}
