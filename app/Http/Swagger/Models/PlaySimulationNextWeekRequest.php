<?php

/**
 * @OA\Schema(
 *     description="Play next week",
 *     type="object",
 *     title="/simulation-play-next-week",
 *     required={"season_id", "week_id"}
 * )
 */
class PlaySimulationNextWeekRequest
{
    /**
     * @OA\Property(
     *     title="Season id",
     *     description="Season id field",
     *     format="string",
     *     example="1"
     * )
     *
     * @var int
     */
    public $season_id;

    /**
     * @OA\Property(
     *     title="Week id",
     *     description="Week id field",
     *     format="string",
     *     example="1"
     * )
     *
     * @var int
     */
    public $week_id;
}
