<?php

/**
 * @OA\Schema(
 *     description="Add new team",
 *     type="object",
 *     title="/command",
 *     required={"name", "strength"}
 * )
 */
class AddCommandRequest
{
    /**
     * @OA\Property(
     *     title="Team name",
     *     description="Team name field",
     *     format="string",
     *     example="test"
     * )
     *
     * @var string
     */
    public $name;

    /**
     * @OA\Property(
     *     title="Team strength",
     *     description="Team strength field",
     *     format="int",
     *     example="5"
     * )
     *
     * @var int
     */
    public $strength;
}
