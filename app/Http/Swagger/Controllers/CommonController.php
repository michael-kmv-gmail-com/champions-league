<?php

class CommonController
{
    /**
     * @OA\Get(
     *     path="/health",
     *     operationId="CommonController.health",
     *     tags={"General"},
     *     summary="System health check",
     *     @OA\Response(
     *         response="200",
     *         description="Everything is fine",
     *         @OA\JsonContent()
     *     ),
     *     @OA\Response(
     *         response="500",
     *         description="Something wrong"
     *     )
     * )
     * Health api
     */
    public function health()
    {
    }
}
