<?php

/**
 * Manager class for external usage
 */

namespace App\Containers\Helpers;

use App\Containers\Helpers\Tasks\RandomlySwapArray;
use App\Containers\Helpers\Tasks\GeneratePairs;
use App\Containers\Helpers\Tasks\CheckWithProbability;

class HelpersManager
{
    /**
     * Randomly swap values in array
     *
     * @return array
     */
    public function randomlySwapArray($array)
    {
        return app(RandomlySwapArray::class)->randomlySwapArray($array);
    }

    /**
     * Generate pairs based on input number
     *
     * @param int $num
     * @return array
     */
    public function generatePairs($num)
    {
        return app(GeneratePairs::class)->generatePairs($num);
    }

    /**
     * Single probability check with linear probability
     *
     * @param float $probability
     * @param int $length
     * @return boolean
     */
    public function checkWithProbability($probability=0.1, $length=10000)
    {
        return app(CheckWithProbability::class)->checkWithProbability($probability, $length);
    }
}
