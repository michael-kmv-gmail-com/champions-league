<?php

namespace App\Containers\Helpers\Tasks;

class RandomlySwapArray
{
    /**
     * Randomly swap values in array
     *
     * @return array
     */
    public function randomlySwapArray($array)
    {
        shuffle($array);
        shuffle($array);
        return $array;
    }
}
