<?php

namespace App\Containers\Helpers\Tasks;

class GeneratePairs
{
    /**
     * Generate pairs based on input number
     *
     * @param int $n
     * @return array
     */
    public function generatePairs($n)
    {
        $result = [];
        for ($i = 0; $i < $n; $i++)
            for ($x = 0; $x < $n; $x++)
                if ($i != $x)
                    $result[] = [$i, $x];
        return $result;
    }
}
