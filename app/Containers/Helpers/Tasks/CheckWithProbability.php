<?php

namespace App\Containers\Helpers\Tasks;

class CheckWithProbability
{
    /**
     * Single probability check with linear probability
     *
     * @param float $probability
     * @param int $length
     * @return boolean
     */
    public function checkWithProbability($probability=0.1, $length=9000)
    {
        $test = mt_rand(1, $length);
        return $test<=$probability*$length;
    }
}
