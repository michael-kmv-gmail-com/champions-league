<?php

namespace App\Containers\Commands\Dto;

use Illuminate\Http\Request;
use App\Containers\Commands\Dto\CommandDto;

class CreateCommandDtoFactory
{
    /**
     * @param Request $request
     * @return \App\Containers\Commands\Dto\CommandDto
     */
    public static function fromRequest(Request $request)
    {
        return self::fromArray($request->validated());
    }

    /**
     * @param array $data
     * @return \App\Containers\Commands\Dto\CommandDto
     */
    public static function fromArray(array $data)
    {
        $dto = new CommandDto();
        $dto->name = $data['name'];
        $dto->strength = $data['strength'];

        return $dto;
    }
}
