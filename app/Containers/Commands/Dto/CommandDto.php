<?php
namespace App\Containers\Commands\Dto;


class CommandDto
{
    /**
     * Command name
     * @var string
     */
    public $name;

    /**
     * Command strength
     * @var int
     */
    public $strength;
}
