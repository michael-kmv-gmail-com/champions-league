<?php

namespace App\Containers\Commands\Actions;

use App\Containers\Commands\Tasks\AllCommandsWithOrder;


class CommandsList
{
    /**
     * Get the list of all command ordered by "order"
     *
     * @param string $order
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getAllCommandsList($order = 'ASC')
    {
        return app(AllCommandsWithOrder::class)->getAllCommandsWithOrder($order);
    }
}
