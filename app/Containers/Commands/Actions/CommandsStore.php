<?php

namespace App\Containers\Commands\Actions;

use App\Containers\Commands\Tasks\SaveCommand;
use App\Containers\Commands\Dto\CommandDto;


class CommandsStore
{
    /**
     * Add new command
     *
     * @param string $name
     * @param string $strength
     * @return mixed
     */
    public function saveCommand(CommandDto $dto)
    {
        return app(SaveCommand::class)->saveCommand($dto);
    }
}
