<?php

/**
 * Manager class for external usage
 */

namespace App\Containers\Commands;

use App\Containers\Commands\Actions\CommandsList;
use App\Containers\Commands\Tasks\AllCommandsWithOrder;
use App\Containers\Commands\Tasks\GetCommandsByIds;

class CommandsManager
{
    /**
     * Get the list of all command ordered by "order"
     *
     * @param string $order
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getAllCommandsList($order = 'ASC')
    {
        return app(AllCommandsWithOrder::class)->getAllCommandsWithOrder($order);
    }

    /**
     * Get the list of commands limited by Ids
     *
     * @param array $ids
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getCommandsByIds($ids)
    {
        return app(GetCommandsByIds::class)->getCommandsByIds($ids);
    }


}
