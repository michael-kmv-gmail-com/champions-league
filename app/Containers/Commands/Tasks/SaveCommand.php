<?php

namespace App\Containers\Commands\Tasks;

use App\Containers\Commands\Dto\CommandDto;
use App\Models\Command;

class SaveCommand
{
    /**
     * Save command to the DB
     *
     * @param CommandDto $dto
     * @return Command
     */
    public function saveCommand($dto)
    {
        return Command::create([
            'name' => $dto->name,
            'strength' => $dto->strength,
        ]);
    }
}
