<?php

namespace App\Containers\Commands\Tasks;

use Illuminate\Support\Facades\Cache;
use App\Models\Command;

class AllCommandsWithOrder
{
    /**
     * Get the list of all command from DB ordered by "order"
     *
     * @param string $order
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getAllCommandsWithOrder($order = 'ASC')
    {
        return Cache::remember('AllCommandsWithOrder.getAllCommandsWithOrder.' . $order, config('cache.api.db.short'), function () use ($order) {
            return Command::orderBy('name', $order)->get();
        });
    }
}
