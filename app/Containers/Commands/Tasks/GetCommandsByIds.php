<?php

namespace App\Containers\Commands\Tasks;

use Illuminate\Support\Facades\Cache;
use App\Models\Command;

class GetCommandsByIds
{
    /**
     * Get the list of commands limited by Ids
     *
     * @param array $ids
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getCommandsByIds($ids)
    {
        return Cache::remember('GetCommandsByIds.getCommandsByIds.' . implode('_', $ids), config('cache.api.db.short'), function () use ($ids) {
            return Command::whereIn('id', $ids)->get();
        });
    }
}
