<?php

namespace App\Containers\Simulation\Actions;

use App\Containers\Commands\CommandsManager;
use App\Containers\Simulation\Dto\CreateSeasonDtoFactory;
use App\Containers\Simulation\Tasks\InitSeason;
use App\Containers\Simulation\Tasks\RandomlySwapCommandIds;
use App\Containers\Simulation\Tasks\DrawForTeams;
use App\Containers\Simulation\Tasks\GenerateWeeklyGamePlan;
use App\Containers\Simulation\Tasks\SaveNewSeason;


class SimulationInit
{
    /**
     * Init simulation: create new season, do draw, store info
     *
     * @param string $order
     * @return array
     */
    public function initSumulation()
    {
        // Create new season
        $seasonData = app(InitSeason::class)->init();

        // Get Ids list of commands
        $commandsIds = app(CommandsManager::class)->getAllCommandsList()->pluck('id')->toArray();
        // Swap Ids list of commands
        $commandsIds = app(RandomlySwapCommandIds::class)->randomSwapIds($commandsIds);

        // Teams pairs
        $draw = app(DrawForTeams::class)->draw($commandsIds);

        // Weekly game plan
        $weeks = app(GenerateWeeklyGamePlan::class)->generateGamePlan($draw, true);
        if (count(end($weeks)) != 2) $weeks = app(GenerateWeeklyGamePlan::class)->generateGamePlan($draw, false);

        // Swap weekly game plan
        $weeks = app(RandomlySwapCommandIds::class)->randomSwapIds($weeks);

        // Save simulation
        $seasonDto = CreateSeasonDtoFactory::fromArray(['season_name' => $seasonData, 'weekly_game_plan' => $weeks]);
        $season = app(SaveNewSeason::class)->saveSeason($seasonDto);

        return $season->toArray();
    }
}
