<?php

namespace App\Containers\Simulation\Actions;

use App\Containers\Simulation\Dto\PlaySimulationNextWeekDto;
use App\Containers\Simulation\Tasks\GetSeasonById;
use App\Containers\Simulation\Tasks\GetSeasonWeekGames;
use App\Containers\Simulation\Tasks\PlayExactGame;
use App\Containers\Commands\CommandsManager;
use App\Containers\Simulation\Dto\CreateMatchDtoFactory;

class SimulationPlay
{
    /**
     * Validate play simulation next week data
     *
     * @param PlaySimulationNextWeekDto $dto
     * @return boolean
     */
    public function isValidSimulationPlayData($dto)
    {
        $season = app(GetSeasonById::class)->getSeasonById($dto->season_id);
        $games = app(GetSeasonWeekGames::class)->getGamesBySeasonWeek($dto->season_id, $dto->week_id);

        return ($season && !$games->count());
    }

    /**
     * Play next week simulation
     *
     * @param PlaySimulationNextWeekDto $dto
     * @return array of \App\Containers\Simulation\Dto\MatchDto
     * @throws \Exception
     */
    public function playSumulationNextWeek($dto)
    {
        $season = app(GetSeasonById::class)->getSeasonById($dto->season_id);
        try {
            $weeklyGamePlan = json_decode($season->weekly_game_plan, true);
            $match = $weeklyGamePlan[$dto->week_id]['match'];
            $commands = app(CommandsManager::class)->getCommandsByIds([$match[0][0], $match[0][1], $match[1][0], $match[1][1]]);
            $playedMatches = [
                $this->playExactGame($commands, $match, 0),
                $this->playExactGame($commands, $match, 1),
            ];
            return $playedMatches;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    private function playExactGame($commands, $match, $num)
    {
        $dto = CreateMatchDtoFactory::fromArray([
            'command1_id' => $commands->where('id', $match[$num][0])->pluck('id')[0],
            'command1_strength' => $commands->where('id', $match[$num][0])->pluck('strength')[0],
            'command1_win' => false,
            'command1_drawn' => false,
            'command1_goals' => 0,
            'command2_id' => $commands->where('id', $match[$num][1])->pluck('id')[0],
            'command2_strength' => $commands->where('id', $match[$num][1])->pluck('strength')[0],
            'command2_win' => false,
            'command2_drawn' => false,
            'command2_goals' => 0,
        ]);
        return app(PlayExactGame::class)->playGame($dto);
    }

}
