<?php

namespace App\Containers\Simulation\Tasks;

class GenerateWeeklyGamePlan
{
    /**
     * Generate weekly game plan for teams
     *
     * [0] => Array
     *   (
     *       [match] => Array
     *           (
     *               [0] => Array
     *                   (
     *                       [0] => 4
     *                       [1] => 3
     *                   )
     *
     *               [1] => Array
     *                   (
     *                       [0] => 2
     *                       [1] => 1
     *                   )
     *
     *           )
     *
     *   )
     *
     * @param array $draw (array of team pairs)
     * @param boolean $doWeekTeamsCheck
     * @return array
     */
    public function generateGamePlan($draw, $doWeekTeamsCheck)
    {
        $weeksCnt = (int)count($draw) / 2;
        $weeks = [];
        $selectedPairs = [];

        for ($i = 0; $i < $weeksCnt; $i++) {
            $weeks[$i] = ['match' => []];

            for ($j = 0; $j < count($draw); $j++) {
                if (in_array($j, $selectedPairs)) continue;
                // first match
                $weeks[$i]['match'][0] = $draw[$j];
                $selectedPairs[] = $j;
                // second match
                for ($k = 0; $k < count($draw); $k++) {
                    if (in_array($k, $selectedPairs)) continue;
                    // no need pair with team which already in match 0
                    if ( $doWeekTeamsCheck && (in_array($draw[$k][0], $weeks[$i]['match'][0]) || in_array($draw[$k][1], $weeks[$i]['match'][0]))) continue;
                    $weeks[$i]['match'][1] = $draw[$k];
                    $selectedPairs[] = $k;
                    break;
                }
                if (count($weeks[$i]['match']) == 2) break;
            }
        }

        return $weeks;
    }
}
