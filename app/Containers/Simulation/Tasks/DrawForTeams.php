<?php

namespace App\Containers\Simulation\Tasks;

use App\Containers\Helpers\HelpersManager;

class DrawForTeams
{
    /**
     * Generate teams pairs based on teams ids list
     * [0 (match)] => Array
     *       (
     *           [0] => 8 (command id)
     *           [1] => 7 (command id)
     *       )
     *
     * @param array $commandsIds
     * @return array
     */
    public function draw($commandsIds)
    {
        $pairs = app(HelpersManager::class)->generatePairs(count($commandsIds));

        $draw = [];
        foreach ($pairs as $pair) {
            $draw[] = [$commandsIds[$pair[0]], $commandsIds[$pair[1]]];
        }

        return $draw;
    }
}
