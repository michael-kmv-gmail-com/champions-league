<?php

namespace App\Containers\Simulation\Tasks;

use App\Models\Game;

class GetSeasonWeekGames
{
    /**
     * Get Game by season and week
     *
     * @param int $seasonId
     * @param int $weekNum
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getGamesBySeasonWeek($seasonId, $weekNum)
    {
        return Game::where('season_id', $seasonId)->where('week_num', $weekNum)->get();
    }
}
