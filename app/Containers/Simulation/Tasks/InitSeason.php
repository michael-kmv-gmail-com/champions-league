<?php

namespace App\Containers\Simulation\Tasks;

class InitSeason
{
    /**
     * Init new season
     *
     * @return string
     */
    public function init()
    {
        return date("Y-m-d H:i:s");
    }
}
