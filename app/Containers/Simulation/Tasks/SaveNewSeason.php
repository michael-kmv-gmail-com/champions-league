<?php

namespace App\Containers\Simulation\Tasks;

use App\Containers\Simulation\Dto\SeasonDto;
use App\Models\Season;

class SaveNewSeason
{
    /**
     * Save command to the DB
     *
     * @param SeasonDto $dto
     * @return Season
     */
    public function saveSeason($dto)
    {
        return Season::create([
            'name' => $dto->season_name,
            'weekly_game_plan' => json_encode($dto->weekly_game_plan),
        ]);
    }
}
