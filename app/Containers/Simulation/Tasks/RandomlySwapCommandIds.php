<?php

namespace App\Containers\Simulation\Tasks;

use App\Containers\Helpers\HelpersManager;

class RandomlySwapCommandIds
{
    /**
     * Randomly swap values in array (command ids)
     *
     * @return array
     */
    public function randomSwapIds($commandsIds)
    {
        return app(HelpersManager::class)->randomlySwapArray($commandsIds);
    }
}
