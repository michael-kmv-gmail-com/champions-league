<?php

namespace App\Containers\Simulation\Tasks;

use App\Containers\Simulation\Dto\MatchDto;
use App\Containers\Helpers\HelpersManager;

class PlayExactGame
{
    /**
     * Play game by command data
     *
     * @param MatchDto $dto
     * @return MatchDto $dto
     */
    public function playGame($dto)
    {
        // Define winner or drawn
        $k = 0;
        while ($dto->command1_win === $dto->command2_win) {
            $dto->command1_win = app(HelpersManager::class)->checkWithProbability($dto->command1_strength / 10);
            $dto->command2_win = app(HelpersManager::class)->checkWithProbability($dto->command2_strength / 10);
            $k++;
        }
        if ($k > 2) {
            // drawn
            $dto->command1_win = $dto->command2_win = false;
            $dto->command1_drawn = $dto->command2_drawn = true;
            $dto->command1_goals = $dto->command2_goals = $this->getRandomGoal();
        }
        else {
            $goals = [0, 0];
            while ($goals[0] === $goals[1]) {
                $goals = [$this->getRandomGoal(), $this->getRandomGoal()];
            }
            sort($goals);
            $i = ($dto->command1_win) ? 1 : 0;
            $dto->command1_goals = $goals[(int)$i];
            $dto->command2_goals = $goals[(int)!$i];
        }
        return $dto;
    }

    private function getRandomGoal()
    {
        return mt_rand(0, 9);
    }

}
