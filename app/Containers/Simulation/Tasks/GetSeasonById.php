<?php

namespace App\Containers\Simulation\Tasks;

use Illuminate\Support\Facades\Cache;
use App\Models\Season;

class GetSeasonById
{
    /**
     * Get season by id
     *
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getSeasonById($id)
    {
        return Cache::remember('getSeasonById.getSeasonById.' . $id, config('cache.api.db.long'), function () use ($id) {
            return Season::find($id);
        });
    }
}
