<?php

namespace App\Containers\Simulation\Dto;

use Illuminate\Http\Request;
use App\Containers\Simulation\Dto\MatchDto;

class CreateMatchDtoFactory
{
    /**
     * @param Request $request
     * @return \App\Containers\Simulation\Dto\MatchDto
     */
    public static function fromRequest(Request $request)
    {
        return self::fromArray($request->validated());
    }

    /**
     * @param array $data
     * @return \App\Containers\Simulation\Dto\MatchDto
     */
    public static function fromArray(array $data)
    {
        $dto = new MatchDto();
        $dto->command1_id = $data['command1_id'];
        $dto->command1_strength = $data['command1_strength'];
        $dto->command1_win = $data['command1_win'];
        $dto->command1_goals = $data['command1_goals'];
        $dto->command2_id = $data['command2_id'];
        $dto->command2_strength = $data['command2_strength'];
        $dto->command2_win = $data['command2_win'];
        $dto->command2_goals = $data['command2_goals'];

        return $dto;
    }
}
