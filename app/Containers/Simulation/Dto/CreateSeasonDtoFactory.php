<?php

namespace App\Containers\Simulation\Dto;

use Illuminate\Http\Request;
use App\Containers\Simulation\Dto\SeasonDto;

class CreateSeasonDtoFactory
{
    /**
     * @param Request $request
     * @return \App\Containers\Simulation\Dto\SeasonDto
     */
    public static function fromRequest(Request $request)
    {
        return self::fromArray($request->validated());
    }

    /**
     * @param array $data
     * @return \App\Containers\Simulation\Dto\SeasonDto
     */
    public static function fromArray(array $data)
    {
        $dto = new SeasonDto();
        $dto->season_name = $data['season_name'];
        $dto->weekly_game_plan = $data['weekly_game_plan'];

        return $dto;
    }
}
