<?php

namespace App\Containers\Simulation\Dto;

use Illuminate\Http\Request;
use App\Containers\Simulation\Dto\PlaySimulationNextWeekDto;

class CreatePlaySimulationNextWeekDtoFactory
{
    /**
     * @param Request $request
     * @return \App\Containers\Simulation\Dto\PlaySimulationNextWeekDto
     */
    public static function fromRequest(Request $request)
    {
        return self::fromArray($request->validated());
    }

    /**
     * @param array $data
     * @return \App\Containers\Simulation\Dto\PlaySimulationNextWeekDto
     */
    public static function fromArray(array $data)
    {
        $dto = new PlaySimulationNextWeekDto();
        $dto->season_id = $data['season_id'];
        $dto->week_id = $data['week_id'];

        return $dto;
    }
}
