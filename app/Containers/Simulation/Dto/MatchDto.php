<?php
namespace App\Containers\Simulation\Dto;


class MatchDto
{
    /**
     * Command 1 id
     * @var int
     */
    public $command1_id;

    /**
     * Command 1 strength
     * @var int
     */
    public $command1_strength;

    /**
     * Command 1 win
     * @var bool
     */
    public $command1_win = false;

    /**
     * Command 1 drawn
     * @var bool
     */
    public $command1_drawn = false;

    /**
     * Command 1 win
     * @var bool
     */
    public $command1_goals = 0;

    /**
     * Command 2 id
     * @var int
     */
    public $command2_id;

    /**
     * Command 2 strength
     * @var int
     */
    public $command2_strength;

    /**
     * Command 2 win
     * @var bool
     */
    public $command2_win = false;

    /**
     * Command 1 drawn
     * @var bool
     */
    public $command2_drawn = false;

    /**
     * Command 1 win
     * @var bool
     */
    public $command2_goals = 0;
}
