<?php
namespace App\Containers\Simulation\Dto;


class PlaySimulationNextWeekDto
{
    /**
     * Season id
     * @var int
     */
    public $season_id;

    /**
     * Week id
     * @var int
     */
    public $week_id;
}
