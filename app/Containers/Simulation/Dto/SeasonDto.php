<?php
namespace App\Containers\Simulation\Dto;


class SeasonDto
{
    /**
     * Season name
     * @var string
     */
    public $season_name;

    /**
     * Weekly plan
     * @var array
     *       [0] => Array
     *           (
     *               [match] => Array
     *                   (
     *                       [0] => Array
     *                           (
     *                               [0] => 4
     *                               [1] => 3
     *                           )
     *
     *                       [1] => Array
     *                           (
     *                               [0] => 2
     *                               [1] => 1
     *                           )
     *
     *                   )
     *
     *           )
     */
    public $weekly_game_plan;
}
