<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\CommandsController;
use App\Http\Controllers\Api\SimulationController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('header-access-control-allow-origin')->group( function () {
    // Teams
    Route::get('commands',  [CommandsController::class, 'index']);
    Route::post('command', [CommandsController::class, 'store']);

    // Simulation
    Route::post('simulation-run', [SimulationController::class, 'run']);
    Route::post('simulation-play-next-week', [SimulationController::class, 'playNextWeek']);
});

// A route for health check
Route::get('health', function () {
    return 'OK';
});
